import java.util.Scanner;

public class ex1 {

    public static int fib(int n){
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else  {
            return fib(n - 1) + fib(n - 2);
        }
    }

    public static void main(String[] args) {
        int value;

        Scanner number = new Scanner(System.in);

        System.out.print("Введите число последовательности до которого хотите получить: ");
        value = number.nextInt();

        int[] arr = new int[value];
        arr[0] = 0;
        arr[1] = 1;
        for(int i = 0; i < value; i++) {
            System.out.println(fib(i));
        }
    }
}
